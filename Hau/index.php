<?php
// $link = ['c' => 'login', 'a' => 'show'];//$link là một mảng để lưu cái tên giá trị controlle và tến giá trị action
// echo http_build_query($link); //tao ra chuoi c=login&a=show
// die;
ob_start();
session_start();
function __autoload($className) {
	if (strpos($className, 'Controller')) {
		$className = 'Controllers/' . $className;
	} else {
		$className = str_replace('_', '/', $className);
	}
	$className .= '.php';

	// kiểm tra file có tồn tại hay không
	if (file_exists($className)) {

		include_once $className;
	} else {
		echo "loi";
	}

}
if (isset($_GET['c'])) {
	$controllerName = $_GET['c'];
} else {
	$controllerName = 'login';
}
if (isset($_GET['a'])) {
	$action = $_GET['a'];
} else {
	$action = 'login';
}
$controllerName = ucfirst($controllerName) . 'Controller';
$controller = new $controllerName;

if (!method_exists($controller, $action)) {
	echo "không có phương thức trong lớp";
} else {
	echo $controller->$action();
}
?>