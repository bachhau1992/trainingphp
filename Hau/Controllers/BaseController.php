<?php
/**
 *
 */
class BaseController {

	public function Redirect($link = []) {
		$link = http_build_query($link); //
		header("location:index.php?" . $link);
	}

	public function hasSession($session) {
		if (isset($_SESSION[$session])) {
			return true;
		} else {
			return false;
		}
	}
	public function setSession($session, $valueSession) {
		return $_SESSION[$session] = $valueSession;
	}
	public function getSession($session) {
		return $_SESSION[$session];
	}

	public function deleteSession($session) {
		if ($this->hasSession($session)) {
			unset($_SESSION[$session]);
		}
	}
	public function render($viewPath, $data = []) {
		ob_start(); //mo mot vung nho của doi tượng (data)
		extract($data); //giải nén dữ liệu rồi lưu dât vào vùng nhớ của đôi tượng vừa tạo
		include_once 'Views/' . $viewPath;
		$result = ob_get_contents(); //lây các dữ liệu của đối tượng gán vào biến ket qua
		ob_end_clean(); //lấy xong rồi xóa bỏ vùng nhớ và trả về kết quả;
		return $result;
	}
}
?>