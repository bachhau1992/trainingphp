<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title> login</title>
	<link rel="stylesheet" href="">
</head>
<body>
	<form action="index.php?c=login&a=login" method="POST" accept-charset="utf-8">
		<table>
			<tr>
				<?php
if ($this->hasSession('errors_validate')) {
	$errorsValidate = $this->getSession('errors_validate');
	$this->deleteSession('errors_validate');
}
?>
			</tr>
			<tr>
				<td colspan="2">login</td>
			</tr>
			<tr>
				<td>Email</td>
				<td><input type="text" name="txtemail"></td>
				<td><?php if (isset($errorsValidate)) {
	echo $errorsValidate['txtemail'];
}
?></td>
			</tr>
			<tr>
				<td>Pass</td>
				<td><input type="text" name="txtpass"></td>
				<td><?php if (isset($errorsValidate)) {
	echo $errorsValidate['txtpass'];
}
?></td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="submit" name="btlogin" value="Login">
				</td>
			</tr>
		</table>
	</form>
</body>
</html>