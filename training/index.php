<?php
ob_start();
session_start();
function __autoload($className) {

	if (strpos($className, 'Controller')) {
		$className = 'Controllers/' . $className;
	} else {
		$className = str_replace('_', '/', $className);
	}
	$className .= '.php';


	if (file_exists($className)) {
		include_once $className;

	} else {
		echo "loi roi";
	}
}

if (isset($_GET['c'])) {
	$controllerName = $_GET['c'];
} else {
	$controllerName = 'login';
}
if (isset($_GET['a'])) {
	$actionName = $_GET['a'];
} else {
	$actionName = 'login';
}
$controllerName = ucfirst($controllerName) . 'Controller';
$controller = new $controllerName();
if (!method_exists($controller, $actionName)) {
	$actionName = 'notFound';
}
echo $controller->$actionName();

