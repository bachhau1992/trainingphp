<?php
/**
 *
 */
class Models_Admin extends Models_Base {
	protected $tableName = 'admin';

	public function login($where) {
		$this->where($this->tableName, $where);
		return $this->count();
	}

    public function getAll(){
        $this->where($this->tableName);
        return $this->getData();
    }
    public function insertAdmin($data){
        return $this->insert($this->tableName,$data);
    }

    public function findByEmail($email)
    {
        $this->where($this->tableName, $email);
        return $this->count() > 0 ? true : false;
    }
}

//

/**
    return $this->count() > 0 ? true : false; 
    tuong duong voi
    if($this->count() > 0) {
        return true;
    }else {
        return false;
    }
    
*/
