<?php
require "Config/Database.php";

/**
 *
 */
class Models_Base extends Database {

	function __construct() {
		parent::__construct(); // gọi tới function construct của thằng cha mà ko sửa function đó.
	}

	/**
	 * Query Sql with params $sql
	 *
	 */
	public function query($sql) {
		$this->result = mysqli_query($this->connect, $sql);
	}

	/**
	 * Function tim kiem voi cac dieu kien where
	 *
	 */
	// public function find($condition = ['id' => 1, 'role' => 'admin', 'password' => '**']) {
	// 	Select * from user where id = 1 and role = admin and password = **
	// }
	// ['id' => 1]
	// ['id' => 1, 'role' => 'admin', 'password' => '**']

	// ham nay dung de selct co dieu kien
	public function where($table, $condition = ['1' => 1], $order = []) {
		$sql = "SELECT * FROM $table WHERE";
		$i = -1;
		foreach ($condition as $key => $value) {
			$i++;
			if ($i == count($condition) - 1) {
				$sql .= " $key = '" . $value . "'";
			} else {
				$sql .= " $key = '" . $value . "' AND";
			}
		}

		// Dung de order
		if (count($order) > 0) {
			$sql .= "ORDER BY ";
		}
		foreach ($order as $key => $value) {
			$i++;
			if ($i == count($condition) - 1) {
				$sql .= "$key $value";
			} else {
				$sql .= "$key $value ,";
			}
		}
		// echo $sql;
		// die;
		$this->query($sql);
	}


// 	public function getTabkle($table){
	// 		$sql = "SELECT * FROM $table";
	// 		return $this->query($sql);
	// 	}
	// 	where('user');
	// SELECT * FROM user WHERE 1 =1
	//thay vi viet ham getTabkle  thif se gans $condition = ['1' => 1] cos nghia dieu khien 1=1 luon dung.

	public function count() {
		if ($this->result) {
			$row = mysqli_num_rows($this->result);
			return $row;
		}
	}

	public function getData() {
		$data = [];
		while ($row = mysqli_fetch_assoc($this->result)) {
		   $data[] = $row;
		}
		return $data;
	}

	public function insert(string $table, array $data = []) : bool {
		$sql ='';
		$fieldSql = '';
		$valueSql = '';
		$i = -1;
		foreach ($data as $field => $value) {
			$i++;
			if($i == count($data) - 1) {
				$fieldSql .= $field;
				$valueSql .= "'" . $value . "'";
			}else {
				$fieldSql .= $field . ',';
				$valueSql .=  "'" . $value . "'" . ',';
			}
		}
		$sql ="INSERT INTO $table($fieldSql)VALUES($valueSql)";
		if ($this->query($sql)) {
			return true;
		}else{
			return false;
		}
	}

}
