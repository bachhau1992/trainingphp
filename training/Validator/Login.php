<?php
/**
 *
 */
class Validator_Login extends Validator_Base {
	public function validate($data = []) {
		if ($this->required($data['txtemail'])) {
			$this->addErrors('txtemail', "email khong duoc de trong");
		}
		if ($this->required($data['txtpass'])) {
			$this->addErrors('txtpass', "mat khau khong duoc de trong");
		}
		if (!$this->required($data['txtemail']) && $this->checkEmail($data['txtemail'])) {
			$this->addErrors('txtemail', "emai khong cos kys tu @");
		}
		//neu ma khong loi thi return true
		if (empty($this->errors)) {
			return true;
		} else {
			return false;
		}
	}
}