<?php
/**
 *
 */
class Validator_AddAdmin extends Validator_Base {
	public function validate($data = []) {
		if ($this->required($data['txtname'])) {
			$this->addErrors('txtname', "name khong duoc de trong");
		}
		if ($this->required($data['txtemail'])) {
			$this->addErrors('txtemail', "email khong duoc de trong");
		}
		if ($this->required($data['txtpass'])) {
			$this->addErrors('txtpass', "mat khau khong duoc de trong");
		}
		if (!$this->required($data['txtemail']) && $this->checkEmail($data['txtemail'])) {
			$this->addErrors('txtemail', "emai khong cos kys tu @");
		}
		if ($this->required($data['txtpassconfirmation'])) {
			$this->addErrors('txtpassconfirmation', "re pass khong duoc de trong");
		}
		if(!$this->required($data['txtpass']) && !$this->required($data['txtpassconfirmation'])) {
			if ($this->checkSamePassword($data['txtpass'], $data['txtpassconfirmation'])) {
				$this->addErrors('errorPass', "mat khau khong turng khop");
			}
		}
		//neu ma khong loi thi return true
		if (empty($this->errors)) {
			return true;
		} else {
			return false;
		}
	}
}