<?php
/**
 *
 */
class Validator_Base {
	protected $errors = [];

	public function required($field) {
		if (empty($field)) {
			return true;
		} else {
			return false;
		}
	}

	public function checkFile($file) {
		if ($_FILES[$file]['type'] == 'image/jpeg' || $_FILES[$file]['type'] == 'image/png' || $_FILES[$file]['type'] == 'image/jpg' || $_FILES[$file]['type'] == 'image/gif') {
			return true;
		} else {
			return false;
		}
	}
	public function checkSize($file) {
		if ($_FILES[$file]['size'] < 8000) {
			return true;
		} else {
			return false;
		}
	}

	public function addErrors($field, $message) {
		$this->errors[$field] = $message;
	}

	public function getErrors() {
		return $this->errors;
	}

	public function checkEmail($field) {
		if (strpos($field, '@') < 1) {
			return true;
		} else {
			return false;
		}
	}

	public function checkSamePassword($pass, $rePass)
	{
		return $pass != $rePass ? true : false;
	}
}