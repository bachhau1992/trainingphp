
 <!DOCTYPE html>
<html>
<head>
    <title>ADMIN</title>
    <link rel="stylesheet" type="text/css" href="../public/css/style.css"/>
</head>
<body>
        <?php
            //ddaau tien de lay duoc erro thi di kiem co sessionloi do khong
            if ($this->checkSession('validateError')) {
            //neu cos thi se gan vao bien de luu va xoa cai session de khi load lai se mat di
            $validateError = $this->getSession('validateError');
            $this->DeleteSession('validateError');
            echo "<ul>";
            foreach ($validateError as $key => $error) {
              echo "<li>" . $error . "</li>";
            }
            echo "</ul>";
            }
            if ($this->checkSession('message')) {
            echo $this->getSession('message');
            $this->DeleteSession('message');
            }
          ?>

<div class="container">
    <div class="top">
      <a href="index.php?c=login&a=logout">logout</a>
    </div>
    <div class="left">
        <div class="avata">

        </div>
        <ul>
          <li><a href=""><?php echo isset($value['name']) ? $value['name'] : '';?></a></li>
          <li><a href="index.php?c=admin&a=index">List Admin</a></li>
          <li><a href="index.php?c=admin&a=insertAdmin">add New Admin</a></li>
          <li><a href="index.php?c=user&a=index">User</a></li>
        </ul>
      </div>
    <div class="right">
        <form method="POST" action="index.php?c=admin&a=addAdmin" enctype="multipart/form-data">
          <div class="container_formData">
             <div class="field">
               <label>Name </label>
               <span><input type="text" name="txtname"></span>
             </div>
             <div class="field">
               <label>password</label>
               <span><input type="text" name="txtpass"></span>
             </div>
             <div class="field">
               <label>Confirmation password</label>
               <span><input type="text" name="txtpassconfirmation"></span>
             </div>
             <div class="field">
               <label>Email</label>
               <span><input type="text" name="txtemail"></span>
             </div>
             <div class="field">
               <label>Avata</label>
               <span><input type="file" name="txtavata"></span>
             </div>
             <div class="field">
               <label>Role</label>
               <span>
                 <select name="slrole" class="edit-sl">
                   <option>---select status---</option>
                   <option value="2">Super admin</option>
                   <option value="1">Admin</option>
                 </select>
               </span>
             </div>
             <div class="field">
               <span><input class="butonInsert" type="submit" name="btInsert" value="Insert"></span>
             </div>
          </div>
        </form>
    </div>
</div>
</body>
</html>
