<?php
/**
 *
 */
class LoginController extends BaseController {
	public function show() {
		$this->render('login.php');
	}
	public function login() {

		// echo "<pre>";
		// print_r($_POST);
		// die;
		//check validate neu ma thoa man validate thi di tiep con khong thi se hien thi loi;
		$validate = new Validator_Login();
		if ($validate->validate($_POST)) {
			// echo "ok";
			// khoi tao mot lop model
			$admin = new Models_Admin();
			$row = $admin->login(['email' => $_POST['txtemail'], 'password' => md5($_POST['txtpass'])]);
			$adminData = $admin-> getData();
			if ($row > 0) {
				$this->setSession('user',$adminData);
				return $this->Ridirect(['c' => 'user', 'a' => 'index']);
			} else {
				$this->setSession('messageLogin', 'không đúng email hoặc mat khẩu');
				return $this->Ridirect(['c' => 'login', 'a' => 'show']);
			}
		} else {
			//quay lai vaf hien thi loi sang view
			$this->setSession('validateError', $validate->getErrors());
			return $this->Ridirect(['c' => 'login', 'a' => 'show']);
		}
	}

	public function logout(){
		$this->DeleteSession('user');
		return $this->Ridirect(['c' => 'login','a'=>'show']);
	}


}
