<?php
/**
 *
 */
class AdminController extends BaseController
{
    public function index(){
        $data =$this->admin->getAll();
        $this->render('admin/list.php',$data);

    }

    public function __construct(){
        $admin = new Models_Admin;
        $this->admin = $admin;

    }

    public function insertAdmin(){
        $this->render('admin/add.php');
    }

    public function addAdmin()
    {
        try {
            if(isset($_POST['btInsert'])) {
                $validAddAdmin = new Validator_AddAdmin;
                if($validAddAdmin->validate($_POST)) {
                    if($this->admin->findByEmail(['email' => $_POST['txtemail']])) {
                        //quay lai vaf hien thi loi sang view
                        $this->setSession('message', 'Email da duoc dang ky! vui long nhap email khac');
                        return $this->Ridirect(['c' => 'admin', 'a' => 'insertAdmin']);
                    }else {
                        $data = [
                            'name' => $_POST['txtname'],
                            'password' => md5($_POST['txtpass']),
                            'email' => $_POST['txtemail'],
                            'avatar' => $this->upload('txtavata', 'upload/'),
                            'role_type' => $_POST['slrole']
                        ];
                        if($this->admin->insertAdmin($data)) {
                            $this->setSession('message', 'Them user thanh cong');
                            return $this->Ridirect(['c' => 'admin', 'a' => 'insertAdmin']);
                        }
                    }
                }else {
                    //quay lai vaf hien thi loi sang view
                    $this->setSession('validateError', $validAddAdmin->getErrors());
                    return $this->Ridirect(['c' => 'admin', 'a' => 'insertAdmin']);
                }
                
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }


}
