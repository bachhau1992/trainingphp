<?php
/**
 *
 */
class BaseController {
	 function __construct(){
		if (isset($_GEST['c']) && $_GEST['c'] != 'login') {
			if (!$this->checkSession('user')) {
				$this->setSession('messageLogin' ,'vui lòng đăng nhập để vào quản trị');
				return $this->Ridirect(['c'=>'login','a'=>'login']);
			}
		}
	}

	public function Ridirect($link) {
		$link = http_build_query($link); //http_build_query là cái c=login&a=show
		header("location:index.php?" . $link);
	}
	public function render($viewpath, $data = []) {
		ob_start();
		extract($data);
		include_once 'Views/' . $viewpath;
		$result = ob_get_contents();
		return $result;
	}

	public function setSession($nameSession, $valueSession) {
		$_SESSION[$nameSession] = $valueSession;
	}

	public function getSession($nameSession) {
		return $_SESSION[$nameSession];
	}

	public function DeleteSession($nameSession) {
		unset($_SESSION[$nameSession]);
	}

	public function checkSession($nameSession) {
		if (isset($_SESSION[$nameSession])) {
			return true;
		} else {
			return false;
		}
	}
	public function notFound()
	{
		return $this->render('admin/not_found.php');
	}

	public function upload($fileName,$path){
		if ($_FILES[$fileName]['error'] > 0){
			return false;
		}
		move_uploaded_file($_FILES[$fileName]['tmp_name'], $path . $_FILES[$fileName]['name']);
		return $_FILES[$fileName]['name'];
	}


}
